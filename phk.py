# -*- coding: utf-8 -*-
"""
This will start Python HotKey (PHK).
It will load the user scripts and starts listening to the keyboard
for hotkeys and phrases.
End with: Ctrl-c
https://www.python-course.eu/python_tkinter.php
https://realpython.com/python-type-checking/
 f'strings{dkdk}'
 https://realpython.com/python-f-strings/
TODO: macro recorder [Rec...] icoontje?
TODO: error handling overal.
TODO netjes afsluiten met een key ofzo
TODO: stop/start listening to hotkeys and phrases
"""
import os
import pickle
from app.run import run_once
from app.phk.hotkey import Hotkey
from app.phk.phrase import Phrase
from app.script.load_scripts import LoadScripts
from app.phk.keyboard_listener import KeyboardListener

import app
import app.config as config
log = config.default_logger


class Phk:

    def __init__(self) -> None:
        self.kl = KeyboardListener()

    def start(self) -> None:
        """
        Load the user scripts into the listener and start listening
        to the keyboard.
        """
        try:
            this_dir = os.path.dirname(os.path.realpath(__file__))
            user_script_path = os.path.join(this_dir, app.user.root_folder)
            LoadScripts(self.kl, user_script_path, app.user.context_folder)
            self._write_key_reference_file()
            self.pickle_files()
            print("PHK started.")
            # Activate the listener
            self.kl.start()
            # Stop the listener
            self.kl.stop()
        except KeyboardInterrupt:
            self.kl.stop()
            print("PHK stopped.")

    def reload_scripts(self) -> None:
        """
        Re-Load the user scripts into the listener and start listening
        to the keyboard.
        """
        try:
            self.kl.stop()
            print("PHK stopped.")
            this_dir = os.path.dirname(os.path.realpath(__file__))
            user_script_path = os.path.join(this_dir, app.user.root_folder)
            LoadScripts(self.kl, user_script_path, app.user.context_folder)
            self._write_key_reference_file()
            print("PHK started.")
            # Activate the listener
            self.kl.start()
            # Stop the listener
            self.kl.stop()
        except KeyboardInterrupt:
            self.kl.stop()
            print("PHK stopped.")

    @staticmethod
    def set_userscripts_folder(folder_name: str) -> None:
        """
        Sets the folder where the user scripts are located.
        When this is not specifically set the 'user_default' folder will be used.
        The 'user_default' folder is located under the 'user_root' folder.
        Both constants are defined in app/config/__init__.py
        :param folder_name: Name of the user scripts folder.
        """
        app.user.context_folder = folder_name

    def _write_key_reference_file(self) -> None:
        """
        (Over)Write a text file containing a reference of all
        hotkeys and phrases that are available at this moment.
        This can be used as a (printed) reference.
        """
        phrases = Phrase(self.kl).get_key_reference()
        hotkeys = Hotkey(self.kl).get_key_reference()
        key_reference_file = os.path.join(app.system.phk_run_path,
                                          app.user.root_folder,
                                          app.user.key_reference)
        with open(key_reference_file, "w") as file:
            file.write(phrases + "\n\n" + hotkeys)

    def pickle_files(self) -> None:
        """
        Save the hotkeys and phrases to pickled files for use of the DTM
        """
        hotkey_pickle = app.dtm.hotkeys
        phrase_pickle = app.dtm.phrases
        try:
            with open(hotkey_pickle, "wb") as f:
                pickle.dump(self.kl.hotkeys, f, pickle.HIGHEST_PROTOCOL)
                log.debug("Save pickle: %s", hotkey_pickle)
        except FileNotFoundError:
            log.error(f"Could not save pickle: {hotkey_pickle}", exc_info=True)
        try:
            with open(phrase_pickle, "wb") as f:
                pickle.dump(self.kl.phrases, f, pickle.HIGHEST_PROTOCOL)
                log.debug("Save pickle: %s", phrase_pickle)
        except FileNotFoundError:
            log.error(f"Could not save pickle: {phrase_pickle}", exc_info=True)


if __name__ == "__main__":
    run_once('phk')

