# -*- coding: utf-8 -*-
from pynput.keyboard import Key
import app

class Button:
    """
    Listing of Keys and the representation of the key.
    """
    def __init__(self):
        self.ALT = Key.alt  # <alt>
        self.ALT_GR = Key.alt_gr  # <alt_gr>
        self.ALT_L = Key.alt_l  # <alt_l>
        self.ALT_R = Key.alt_r  # <alt_r>
        self.BACKSPACE = Key.backspace  # <backspace>
        self.CAPSLOCK = Key.caps_lock  # <capslock>
        self.CMD = Key.cmd  # <cmd>
        self.CMD_L = Key.cmd_l  # <cmd_l>
        self.CMD_R = Key.cmd_r  # <cmd_r>
        self.CTRL = Key.ctrl  # <ctrl>
        self.CTRL_L = Key.ctrl_l  # <ctrl_l>
        self.CTRL_R = Key.ctrl_r  # <ctrl_r>
        self.DELETE = Key.delete  # <delete>
        self.DOWN = Key.down  # <down>
        self.END = Key.end  # <end>
        self.ENTER = Key.enter  # <enter>
        self.ESC = Key.esc  # <esc>
        self.F1 = Key.f1  # <f1>
        self.F2 = Key.f2  # <f2>
        self.F3 = Key.f3  # <f3>
        self.F4 = Key.f4  # <f4>
        self.F5 = Key.f5  # <f5>
        self.F6 = Key.f6  # <f6>
        self.F7 = Key.f7  # <f7>
        self.F8 = Key.f8  # <f8>
        self.F9 = Key.f9  # <f9>
        self.F10 = Key.f10  # <f10>
        self.F11 = Key.f11  # <f11>
        self.F12 = Key.f12  # <f12>
        self.F13 = Key.f13  # <f13>
        self.F14 = Key.f14  # <f14>
        self.F15 = Key.f15  # <f15>
        self.F16 = Key.f16  # <f16>
        self.F17 = Key.f17  # <f17>
        self.F18 = Key.f18  # <f18>
        self.F19 = Key.f19  # <f19>
        self.F20 = Key.f20  # <f20>
        self.HOME = Key.home  # <home>
        self.LEFT = Key.left  # <left>
        self.PAGE_DOWN = Key.page_down  # <page_down>
        self.PAGE_UP = Key.page_up  # <page_up>
        self.RIGHT = Key.right  # <right>
        self.SHIFT = Key.shift  # <shift>
        self.SHIFT_L = Key.shift_l  # <shift_l>
        self.SHIFT_R = Key.shift_r  # <shift_r>
        self.SPACE = Key.space  # <space>
        self.TAB = Key.tab  # <tab>
        self.UP = Key.up  # <up>
        # Mac gives errors on some keys
        if app.system.os_name != "Darwin":
            self.INSERT = Key.insert  # <insert>
            self.MENU = Key.menu  # <menu>
            self.NUMLOCK = Key.num_lock  # <numlock>
            self.PAUSE = Key.pause  # <pause>
            self.PRINT_SCREEN = Key.print_screen  # <print_screen>
            self.SCROLL_LOCK = Key.scroll_lock  # <scroll_lock>







