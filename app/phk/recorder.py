# -*- coding: utf-8 -*-
import re
import os
import app
import app.config as config
from app.phk.key_convert import KeyConvert
from typing import Iterator, List
from string import Template
from textwrap import indent
from datetime import datetime
import mouseinfo
from pynput.keyboard import KeyCode, Key
from pynput.keyboard import Listener as KeyboardListener
from pynput.mouse import Listener as MouseListener, Button
from itertools import tee, zip_longest
from app.config.ini import Ini, fallback_dtm, fallback_recorder
import PySimpleGUI as sg

log = config.default_logger

ini = Ini()
pos_x = ini.read_int_value("recorder_pos_y", fallback=fallback_recorder.pos_y)
pos_y = ini.read_int_value("recorder_pos_y", fallback=fallback_recorder.pos_y)
message_timeout = ini.read_int_value("recorder_message_timeout",
                                     fallback=fallback_recorder.message_timeout)
theme = ini.read_string_value("dtm_theme", fallback=fallback_dtm.theme)
sg.theme(theme)


class MacroListener:
    def __init__(self, stop_keys: str) -> None:
        self.active_listener: bool = False
        # set the hotkey to abort the listener
        self.stop_keys: str = stop_keys
        self.stop_key_codes: app.KeyCodes = KeyConvert().get_hotkey_keys(stop_keys)
        self.current_active_keys: set = set()  # Tracks the abort hotkey
        self.old_position: set = mouseinfo.position()  # Last position of the mouse
        self.old_position_time: datetime = datetime.now()
        self.commands: List[str] = []
        self.kv = KeyConvert()

    def activate(self) -> None:
        """Start the listener"""
        self._show_start_message()
        self.active_listener = True

        with MouseListener(on_click=self._on_click,
                           on_scroll=self._on_scroll,
                           on_move=self._on_move) as self.listener:
            with KeyboardListener(on_press=self._on_press,
                                  on_release=self._on_release) as self.listener:
                self.listener.join()
        #self.listener.start()

    def deactivate(self) -> None:
        """Deactivate the listener"""
        self.active_listener = False
        self.listener.stop()
        print("Recording stopped.")

    def _append_command(self, action: str) -> None:
        """
        Add a command to the commands list and log this to the macro log.
        :param action: A string containing the macro command
        """
        self.commands.append(action)
        log.debug(action)

    """ ########## MOUSE LOGGING ########### """
    def _on_move(self, x: int, y: int) -> None:
        """
        If the mouse is hovering (stays longer then X seconds in the same place:
        register a moveto event with the current coordinates.
        If the mouse is not hovering, reset the self.old_position coordinates.
        :param x: x coordinate
        :param y: y coordinate
        """
        if self.active_listener:
            if self._is_mouse_hovering():
                self._append_command(f"m.mouse.moveto.coord(({x}, {y}), "
                                     f"seconds=1)")
            else:
                self.old_position = (x, y)

    def _on_click(self, x: int, y: int, button: Button, pressed: bool) -> None:
        if self.active_listener:
            if pressed:
                btn = str(button).split(".")[1]
                self._append_command(f"m.mouse.click.{btn}(({x}, {y}), clicks=1, "
                                     f"wait=0.5)")
                self.old_position = (x, y)

    def _on_scroll(self, x, y, dx, dy):
        pass
        # TODO: Do I want this in the recorder? It only works for Linux...
        # if self.active_listener:
        #     self.commands.append(f'Mouse scrolled at: ({x}, {y})({dx}, {dy})')

    def _is_mouse_hovering(self, min_secs: int=5) -> bool:
        """
        A mouse is considered hovering if it stays on the same place
        for [min_secs]. This is used to determine if we should register
        a 'moveto' event. Otherwise every change in the (x, y) position would trigger
        a 'moveto' event and the log would be full of useless 'moveto' events.
        """
        is_resting = False
        rested = self._time_diff_in_seconds()
        if rested >= min_secs:
            is_resting = True
            self.old_position_time = datetime.now()
            self.old_position_rested = rested
        return is_resting

    def _time_diff_in_seconds(self) -> int:
        """
        Calculate the amount of seconds between two timestamps
        :return: seconds time difference
        """
        timedelta = datetime.now() - self.old_position_time
        seconds = timedelta.days * 24 * 3600 + timedelta.seconds
        return seconds

    """ ########## KEYBOARD LOGGING ########### """

    def _on_press(self, key: Key) -> None:
        """
        Check on every key press if the abort keys are pressed.
        If not, add to the command list.
        """
        if self.active_listener:
            self.current_active_keys.add(self.kv.lowercase_char(key))
            self._log_keys(key)

    def _on_release(self, key: Key) -> None:
        """
        Remove the active keys from the set.
        """
        self.current_active_keys = set()

    def _log_keys(self, key: Key) -> None:
        """
        After a key press, determine if this was a normal character as part of a text
        or a special character.
        If only one key is pressed, it is considered a normal character and they will
        be typed.
        A <space> in between characters will be replaced by a " ".
        If multiple keys are pressed, these are part of a key combination and will
        be pressed.
        :param key:  The key currently pressed
        """
        # key = self.kv.lowercase_char(key)
        if not self._stop_keys_pressed(key):
            try:
                # more then one key is pressed
                if len(self.current_active_keys) > 1:
                    hotkeys = []
                    for hk in self.current_active_keys:
                        hotkeys.append(self.kv.get_key_representation(hk))
                    # remove the last command, which is now part of the hotkey
                    del self.commands[-1]
                    self._append_command(f"m.keys.press(\"{''.join(hotkeys)}\")")
                # a single key is pressed
                else:
                    key_representation = self.kv.get_key_representation(key)
                    # When a single key is pressed, the space is just a space between
                    # two characters, not part of a hotkey.
                    if key_representation == "<space>":
                        key_representation = " "
                    # this is a single character
                    if self.kv.is_single_character(key_representation):
                        self._append_command(f"m.keys.type(\"{key_representation}\")")
                    # this is a special key
                    else:
                        self._append_command(f"m.keys.press(\"{key_representation}\")")
            except KeyError:
                pass
        else:
            self.current_active_keys = set()
            try:  # Remove the last command which is the abort key press.
                del self.commands[-1]
            except IndexError:
                pass
            try:  # This is for Unit testing when the listener is not started.
                self.listener.stop()
            except AttributeError:
                pass

        # log.debug(self.commands)

    def _stop_keys_pressed(self, key: KeyCode) -> bool:
        """
        Check if the stop keys are pressed.
        :param key: The pressed key
        :return: True of False
        """
        key = self.kv.lowercase_char(key)
        abort = False
        if key in self.stop_key_codes:
            try:
                self.current_active_keys.add(key)
            except KeyError:
                pass
            if all(k in self.current_active_keys for k in self.stop_key_codes):
                abort = True
        return abort

    def _show_start_message(self):
        msg = ""
        msg += ("Recording has started...\n")
        msg += (f"Stop recording with: {self.stop_keys}\n\n")
        msg += ("Note: Do not type too fast so two keys are not pressed ")
        msg += ("at the same time. \n\nThis would register a 'press' event")
        msg += ("where a 'type' event should be registered.")
        print("=====================================================")
        print(msg)
        print("\n=====================================================\n")
        sg.PopupNoButtons(msg, title="PHK Macro recorder", location=(pos_x, pos_y),
                      auto_close_duration=message_timeout, auto_close=True,
                      font=("Calibri", 14)
                      )


class Commands:
    """Clean up the Type commands"""

    TYPE_FUNC = "m.keys.type"

    def __init__(self, commands: list) -> None:
        self.kv = KeyConvert()
        self.new_commands = []
        self.temp_text = []
        self._group_type_commands(commands)

    @staticmethod
    def _look_one_command_ahead(commands: list) -> Iterator:
        """
        Makes a new list of commands where the current command and the
        next command are available in each iteration.
        This is used to look ahead in the commands list for making decissions
        based on the next command.
        :param commands: A list of commands
        :return: The new list with a lookup to the next command.
        """
        try:
            cmd, next_cmd = tee(iter(commands))
            next(next_cmd)
            return zip_longest(cmd, next_cmd)
        except StopIteration:
            pass

    def _group_type_commands(self, commands: list) -> None:
        """
        Groups type commands together for a cleaner script. e.g.:
        m.keys.type("t"); m.keys.type("h"); m.keys.type("e")
        becomes: m.keys.type("the")
        Other commands like the "press" and "mouse" commands are not grouped.
        :param commands: The list of commands to group
        """
        for (cmd, next_cmd) in self._look_one_command_ahead(commands):
            if self.TYPE_FUNC in cmd:
                text = self._get_text_from_command(cmd)
                self.temp_text.append(text)
                try:
                    if self.TYPE_FUNC not in next_cmd:
                        self.add_new_type_command()
                except TypeError:
                    self.add_new_type_command()
            else:
                self.new_commands.append(self._indent_string(cmd))

    def add_new_type_command(self) -> None:
        """
        Add the combined values of the temp_text list as a single "type" command"
        to the new_commands list and empty the temp_text list.
        """
        new_cmd = f'{self.TYPE_FUNC}("{"".join(self.temp_text)}")'
        self.new_commands.append(self._indent_string(new_cmd))
        self.temp_text.clear()

    @staticmethod
    def _indent_string(cmd: str) -> str:
        return indent(cmd, '    ')

    @staticmethod
    def _get_text_from_command(cmd: str) -> str:
        """
        Get the text from a "type" command.
        :param cmd: The command string e.g.: m.keys.type("the text")
        :return: The text in the command  e.g.: "the text"
                 If the command does not make any sense, an empty string is returned.
        """
        try:
            match = re.search('(^.*\")(.*)(\".*$)', cmd)
            chars = match.group(2)
        except (TypeError, AttributeError):
            chars = ""
        return chars


class RecordedMacro:
    def __init__(self, commands: list, play_keys: str) -> None:
        self.play_keys = play_keys  # The keys to trigger playing the recorded macro
        self.template = None
        self.file_content = None
        self._read_template()
        self._substitute_values(commands)
        self.output_file_path = os.path.join(app.system.phk_run_path,
                                             app.user.root_folder,
                                             app.recorder.folder,
                                             app.recorder.file)

    def _read_template(self) -> None:
        """
        Read the template file that is used to for creating the macro.
        """
        template_file_path = os.path.join(app.system.phk_run_path,
                                          app.user.root_folder,
                                          app.recorder.folder,
                                          app.recorder.template)
        try:
            template_file = open(template_file_path)
            self.template = Template(template_file.read())
        except IOError:
            log.error(f"Template: {template_file_path} not found.")
            print(f"ERROR: Template: {template_file_path} not found.")
            print("Please recreate it or download it again from the PHK sources.")

    def _substitute_values(self, commands: list) -> None:
        """
        Substitute the placeholders in the template for the real values.
        :param commands:  The recorded commands
        """
        if self.template:
            template_values = {'play_key': self.play_keys,
                               'script': '\n'.join(commands)
                               }
            self.file_content = self.template.substitute(template_values)

    def write_recorded_macro(self) -> None:
        """Write the macro to a file."""
        try:
            if self.file_content:
                with open(self.output_file_path, "w") as file:
                    file.write(self.file_content)
        except IOError:
            log.error(f"Cannot write recorded macro to: [{self.output_file_path}]")
            print(f"ERROR: Cannot write the recorded macro to: {self.output_file_path}")
            print("Please make sure the path exists and check the user rights.")

    def show_end_message(self) -> None:
        """Print a message that the recording is stopped."""
        msg = ""
        msg += "Recording has stopped.\n"
        msg += f"File written to {self.output_file_path}\n"
        msg += f"Play recording with: {self.play_keys}"
        print("=======================================\n")
        print(msg)
        print("\n=======================================\n")
        sg.PopupOK(msg, title="PHK Macro recorder", location=(pos_x, pos_y),
                   auto_close_duration=5, auto_close=False,
                   font=("Calibri", 14)
                  )


def start(stop_keys: str, play_keys: str) -> None:
    """
    Start the macro recorder.
    :param stop_keys: The key needed to stop the recording.
    :param play_keys: The key needed to start playing the recording.
    """
    ml = MacroListener(stop_keys)
    ml.activate()
    ml.deactivate()
    cmds = Commands(ml.commands)
    rm = RecordedMacro(cmds.new_commands, play_keys)
    rm.write_recorded_macro()
    rm.show_end_message()
