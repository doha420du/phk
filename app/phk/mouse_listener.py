# -*- coding: utf-8 -*-
"""
The mouse listener is responsible for:
- Listening to the mouse events
https://emptypage.jp/notes/pyevent.en.html

"""
from datetime import datetime
from pynput import mouse
import mouseinfo
import app.config as config
log = config.default_logger


class MouseListener:

    def __init__(self):
        self.active_listener = False
        self.old_position = mouseinfo.position()
        self.old_position_time = datetime.now()

    def activate(self) -> None:
        """(Re)Start the mouse listener"""
        log.debug("Mouse listener activated")
        self.active_listener = True
        with mouse.Listener(on_move=self._on_move,
                            on_click=self._on_click,
                            on_scroll=self._on_scroll
                            ) as self.listener:
             self.listener.join()

    def deactivate(self) -> None:
        """Deactivate the listener"""
        log.debug("Mouse listener de-activated")
        self.active_listener = False
        self.listener.stop()

    def _on_move(self, x , y):
        if self.active_listener:
            if self._is_mouse_hovering():
                log.info("Mouse hovered {} sec on: ({}.{})".format(
                                                             self.old_position_rested,
                                                             x, y))
            else:
                self.old_position = (x, y)

    def _on_click(self, x, y, button, pressed):
        if self.active_listener:
            if pressed:
                log.info('Mouse clicked at: ({0}, {1}) with {2}'.format(x, y, button))

    def _on_scroll(self, x, y, dx, dy):
        if self.active_listener:
            log.info('Mouse scrolled at: ({0}, {1})({2}, {3})'.format(x, y, dx, dy))

    def _is_mouse_hovering(self, min_secs: float=5):
        is_resting = False
        rested = self._time_diff_in_seconds()
        if rested >= min_secs:
            is_resting = True
            self.old_position_time = datetime.now()
            self.old_position_rested = rested
        return is_resting

    def _time_diff_in_seconds(self):
        timedelta = datetime.now() - self.old_position_time
        seconds = timedelta.days * 24 * 3600 + timedelta.seconds
        return seconds
