# -*- coding: utf-8 -*-
import app.config as config
from app.phk.key_convert import KeyConvert
log = config.default_logger




class Hotkey:
    """
    All functionality for hotkeys.
    Well almost... In the keyboard listener is a function that checks
    if a hotkey is pressed.
    """

    def __init__(self, kl) -> None:
        self.kl = kl
        self.key_convert = KeyConvert()

    def register(self, action: str, trigger: str = None, value: str = None,
                 description: str=None) -> None:
        """
        Add a hotkey to the hotkeys list located in keyboard_listener.py
        A hotkey list is defined like this:
        [
          [action]
          [trigger]
          [value]
          [hotkey_buttons]
          [description]
        ]
        :param action: Action can be of the types defined in app/__init__.py
        :param trigger: The keys that would trigger a command.
        :param value:  Can be a text or a command, dependent on the action type.
        :param description: A short description of the hotkey.
        """
        self.check_for_duplicates(trigger)
        log.debug(f"Adding '{action}' hotkey:{trigger}")
        # Get the actual key codes based on the hotkey string
        hotkey_buttons = self.key_convert.get_hotkey_keys(trigger)
        self.kl.hotkeys.append([action, trigger, value, hotkey_buttons, description])

    def check_for_duplicates(self, trigger: str):
        """
        Checks for duplicate hotkeys.
        If the hotkey is already present we remove it so we can overwrite it
        with a new one. This is needed to overwrite hotkeys from the default
        folder if needed.
        Used in phk.py
        :param trigger: The string containing the hotkey representation
               e.g.: <ctrl><alt><f4>
        """
        duplicate = False
        duplicate_location = None
        for i in self.kl.hotkeys:
            trig = i[1]
            if trigger == trig:
                duplicate = True
                duplicate_location = self.kl.hotkeys.index(i)
            if duplicate:  # overwrite the hotkey
                log.warning(f"-- Duplicate hotkey found:[{trigger}].")
                print(f"Warning: Duplicate hotkey found:[{trigger}]. "
                      f"Overwriting previous hotkey.")
                del self.kl.hotkeys[duplicate_location]

    def get_key_reference(self) -> str:
        """
        Reports available hotkeys.
        Used in phk.py
        """
        report = ''
        try:
            max_len = str(max(len(x[1]) for x in self.kl.hotkeys))
            formatting = "{:>" + max_len + "} : {}\n"
            report += formatting.format('Trigger', 'Description')
            report += formatting.format('---', '-----------')
            items = []
            for hk in self.kl.hotkeys:
                trigger = hk[1]
                description = hk[4]
                items.append(formatting.format(trigger, description))
            items.sort()
            for item in items:
                report += item
            header = f"{len(items)} Available hotkeys:\n"
        except ValueError:
            header = "No hotkeys available."
        return header + report
