# -*- coding: utf-8 -*-
import pprint
import app

from app.phk.keyboard_listener import KeyboardListener
from app.phk.hotkey import Hotkey

pp = pprint.PrettyPrinter(indent=4)


def test_register():
    kl = KeyboardListener()
    hk = Hotkey(kl)
    assert len(kl.hotkeys) == 0
    hk.register(app.action.run, "<cmd>a", "Test 1", "Description 1")
    assert len(kl.hotkeys) == 1
    # Will overwrite the previous hotkey
    hk.register(app.action.run, "<cmd>a", "Test 2", "Same as the other one")
    assert len(kl.hotkeys) == 1



def test_duplicates():
    kl = KeyboardListener()
    hk = Hotkey(kl)
    assert len(kl.hotkeys) == 0
    hk.register(app.action.run, "<cmd>a", "Test 1", "Description 1")
    assert len(kl.hotkeys) == 1
    # This will remove the phrase with the same trigger so we can overwrite it.
    hk.check_for_duplicates("<cmd>a")
    assert len(kl.hotkeys) == 0
    hk.register(app.action.run, "<cmd>a", "Test 2", "Description 2")
    assert len(kl.hotkeys) == 1
    # pp.pprint(kl.hotkeys)
