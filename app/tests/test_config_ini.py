# -*- coding: utf-8 -*-
import pytest
import os
from app.config.ini import Ini

ini_file = os.path.abspath(os.path.join(os.path.dirname(__file__),
                                        "scripts", "test_config.ini"))


def test_file_found() -> None:
    ini = Ini()
    ini.section = "test"
    assert ini._read_ini_file(ini_file) is True
    assert ini._read_ini_file("does_not_exist.ini") is False


def test_read_string_value() -> None:
    ini = Ini()
    ini.section = "test"
    ini._read_ini_file(ini_file)
    assert ini.read_string_value("string_val", "fallback") == "string"
    assert ini.read_string_value("only_in_default", "fallback") == "default"
    assert ini.read_string_value("only_in_test", "fallback") == "test"
    assert ini.read_string_value("only_in_not_used", "fallback") == "fallback"
    assert ini.read_string_value("XXXXXXXXXXXX", "fallback") == "fallback"


def test_read_int_value() -> None:
    ini = Ini()
    ini.section = "test"
    ini._read_ini_file(ini_file)
    assert ini.read_int_value("int_val", 0) == 100
    assert ini.read_int_value("int_val2", 0) == 0
    assert ini.read_int_value("int_val3", None) is None


def test_read_float_value() -> None:
    ini = Ini()
    ini.section = "test"
    ini._read_ini_file(ini_file)
    assert ini.read_float_value("float_val", 0.1) == 0.5
    assert ini.read_float_value("float_val2", 0.1) == 0.1
    assert ini.read_float_value("float_val3", 0) == 0
    assert ini.read_float_value("float_val4", None) is None
    assert ini.read_float_value("float_val5", "a") == "a"


def test_read_bool_value() -> None:
    ini = Ini()
    ini.section = "test"
    ini._read_ini_file(ini_file)
    assert ini.read_bool_value("bool_val", True) is False
    assert ini.read_bool_value("bool_val2", True) is False
    assert ini.read_bool_value("bool_val3", True) is False
    # This is using an invalid data type and we are returning the fallback value
    assert ini.read_bool_value("bool_val4", True) is True


def test_read_list_value() -> None:
    ini = Ini()
    ini.section = "test"
    ini._read_ini_file(ini_file)
    assert ini.read_list_values("list_val", [1, 2]) == ['a', 'b']
