# -*- coding: utf-8 -*-
import pprint
import app

from pynput.keyboard import KeyCode
from app.phk.keyboard_listener import KeyboardListener
from app.phk.phrase import Phrase

pp = pprint.PrettyPrinter(indent=4)


def test_register():
    kl = KeyboardListener()
    ph = Phrase(kl)
    assert len(kl.phrases) == 0
    ph.register(app.action.expand, ";aa", "Test 1", "Description 1")
    assert len(kl.phrases) == 1
    # Will overwrite the previous phrase
    ph.register(app.action.expand, ";aa", "Test 1", "Same as previous phrase")
    assert len(kl.phrases) == 1


def test_current_phrase_keys():
    kl = KeyboardListener()
    ph = Phrase(kl)
    assert kl.phrase_key_string == ""
    ph.add_phrase_key_string(KeyCode.from_char("a"))
    ph.add_phrase_key_string(KeyCode.from_char("b"))
    ph.add_phrase_key_string(KeyCode.from_char("c"))
    assert kl.phrase_key_string == "abc"
    ph.empty_current_phrase_keys()
    assert kl.phrase_key_string == ""


def test_duplicates():
    kl = KeyboardListener()
    ph = Phrase(kl)
    assert len(kl.phrases) == 0
    ph.register(app.action.expand, ";aa", "Test 1", "Description 1")
    assert len(kl.phrases) == 1
    # This will remove the phrase with the same trigger so we can overwrite it.
    ph.check_for_duplicates(";aa")
    assert len(kl.phrases) == 0
    ph.register(app.action.expand, ";aa", "Test 2", "Description 2")
    assert len(kl.phrases) == 1
    # pp.pprint(kl.phrases)
