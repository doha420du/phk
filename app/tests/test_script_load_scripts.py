# -*- coding: utf-8 -*-
import os
import pprint
from app.phk.keyboard_listener import KeyboardListener
from app.script.load_scripts import LoadScripts

# Load hotkeys from the test/scripts/default directory
kl = KeyboardListener()
this_dir = os.path.dirname(os.path.realpath(__file__))
scripts_dir = os.path.join(this_dir, 'scripts')
LoadScripts(kl, scripts_dir, 'default')
pp = pprint.PrettyPrinter(indent=4)


def test_are_phrases_present():
    # pp.pprint(kl.phrases)
    assert len(kl.phrases) > 0
    assert len(kl.phrases) == 3

def test_are_hotkeys_present():
    # pp.pprint(kl.hotkeys)
    assert len(kl.hotkeys) > 0
    assert len(kl.hotkeys) == 1
