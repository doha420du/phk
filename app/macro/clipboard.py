# -*- coding: utf-8 -*-
import pyperclip
from app.script.action import type_keys

import app.config as config
log = config.macro_logger


class Clipboard:
    """
    Functions to manipulate the system clipboard
    """

    @staticmethod
    def copy(text: str) -> None:
        """
        Copy a text to the clipboard.

        :param text: The text to copy.
        """
        log.debug(f"Clipboard copy: [{text}]")
        pyperclip.copy(text)

    @staticmethod
    def paste() -> None:
        """Print the text from the clipboard to stdout."""
        text = pyperclip.paste()
        log.debug(f"Clipboard paste: [{text}]")
        print(text)

    @staticmethod
    def type() -> None:
        """Type the text from the clipboard."""
        text = pyperclip.paste()
        log.debug(f"Clipboard type: [{text}]")
        type_keys(text)

    @staticmethod
    def get() -> str:
        """
        Get the text from the clipboard as a return value.
        The text will not be typed or pasted.
        """
        text = pyperclip.paste()
        log.debug(f"Clipboard get: [{text}]")
        return text

    @staticmethod
    def empty() -> None:
        """
        Empty the clipboard.
        Note: If you use a clipboard manager, the history of your clipboard is still
        visible in  your clipboard manager.
        """
        pyperclip.copy('')
        log.debug("Clipboard empty.")
