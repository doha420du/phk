# -*- coding: utf-8 -*-
from app.script import Run
# Import the library with 'macro' function.:
import app.macro as m

if m.system.os_platform() == "linux":
    hotkeys = [[Run, "<shift><cmd>f", "firefox", "Start firefox"],
               [Run, "<shift><cmd>o", "libreoffice", "Start office"],
               [Run, "<shift><cmd>v", "gvim", "Start vim"]]

elif m.system.os_platform() == "mac":
    hotkeys = []
    hotkeys.append([Run, "<shift><cmd>f", "open -a Firefox", "Firefox"])
    hotkeys.append([Run, "<shift><cmd>o", "open -a LibreOffice", "Office"])
    hotkeys.append([Run, "<shift><cmd>v", "open -a Vim", "Vim"])

